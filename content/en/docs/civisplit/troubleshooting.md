---
title: "Troubleshooting"
description: "Getting help with CiviSplit"
lead: ""
date: 2022-01-12T01:20:13+01:00
lastmod: 2023-12-12T01:20:13+01:00
draft: false
images: []
menu: 
  docs:
    parent: "civisplit"
weight: 60
toc: true
---

Feel free to post bugs and issues on our Gitlab [gitlab.com/openvideo](https://gitlab.com/openvideo)